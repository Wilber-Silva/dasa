<?php

namespace App\Http\Controllers\InstitutionSite;

use App\Repositorys\AdminContactRepository;
use App\Repositorys\AdminWorkImagesRepository;
use App\Repositorys\AdminWorkRepository;
use App\Repositorys\SiteCountViewsRepository;
use App\Services\SiteCustomerService;
use Illuminate\Http\Request;
use Laracasts\Utilities\JavaScript\JavaScriptFacade;

/**
 * Class SiteIndex
 * @package App\Http\Controllers\InstitutionSite
 */
class Site{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function index(){

        // JavaScriptFacade::put([
        //     "citiesAnswered" => cities_answered()
        // ]);

        // return view("institutionSite");
        
        return view("institutionSite");
    }
    
    function drawing(){
        return view("drawing");
    }
    
    function confirm(){
        return view("confirm");
    }
    function ops(){
        return view("ops");
    }
    function done(){
        return view("done");
    }
    
}