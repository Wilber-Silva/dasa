<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * Interface BasicController
 * @package App\Http\Controllers
 */

interface BasicController{

    /**
     * @param Request $request
     * @description home page
     */
    function index(Request $request);
}