CREATE TABLE admins(
        id INT AUTO_INCREMENT PRIMARY KEY,
        email VARCHAR(255),
        password VARCHAR(255),
        remenber_token VARCHAR(255),
        token VARCHAR(255),
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        deleted_at TIMESTAMP
);

CREATE TABLE site_customer(
        id INT AUTO_INCREMENT PRIMARY KEY,
        `key` VARCHAR(100),
        content LONGTEXT,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        deleted_at TIMESTAMP
);

CREATE TABLE contact(
        id INT AUTO_INCREMENT PRIMARY KEY,
        contact_type VARCHAR(100),
        contact VARCHAR(255),
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        deleted_at TIMESTAMP        
);


CREATE TABLE works(
        id INT AUTO_INCREMENT PRIMARY KEY,
        title VARCHAR(30),
        description LONGTEXT,
        zipcode VARCHAR(100),
        city VARCHAR(100),
        `state` VARCHAR(100),
        district VARCHAR(100),
        street VARCHAR(100),
        address VARCHAR(255),
        make_date DATE,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        deleted_at TIMESTAMP
);

CREATE TABLE work_images(
        id INT AUTO_INCREMENT PRIMARY KEY,
        work_id INT,
        file VARCHAR(255),
        background TINYINT(1),
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        deleted_at TIMESTAMP,
        FOREIGN KEY (work_id) REFERENCES works(id)
);

CREATE TABLE count_views(
        id INT AUTO_INCREMENT PRIMARY KEY,
        agent TEXT,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        deleted_at TIMESTAMP
);