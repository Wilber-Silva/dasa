(function ($) {
  $.validator.addMethod('alphanumeric',function(value,element,param) {
        return this.optional(element) || /^[a-zA-Z0-9]+$/.test(value);
    });
})(jQuery);
