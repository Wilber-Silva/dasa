// anonymous original author, but here's a good source: http://www.tidbits.com.br/colecao-de-metodos-para-o-plugin-validate-do-jquery

(function ($) {
  $.validator.addMethod("equalToIgnoreCase", function (value, element, param) {
        return this.optional(element) || 
             (value.toLowerCase() == $(param).val().toLowerCase());
});
})(jQuery);
