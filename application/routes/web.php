<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'InstitutionSite\Site@index')->name('index');
//Route::get('/', 'InstitutionSite\Site@index')->name('index');


Route::get('/confirm', 'InstitutionSite\Site@drawing')->name("drawing");
Route::get('/drawing', 'InstitutionSite\Site@confirm')->name("confirm");
Route::get('/ops', 'InstitutionSite\Site@ops')->name("ops");
Route::get('/done', 'InstitutionSite\Site@done')->name("done");


/**
 * Group office routes
 */

include "office.php";