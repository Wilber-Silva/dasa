<?php


Route::prefix('/admin')->group(function () {

    Route::get('/', 'Admin\AdminLoginController@index');
    Route::post('/verify', 'Admin\AdminLoginController@login')->name('adminLogin');
    Route::get('/logout', 'Admin\AdminLoginController@index')->name('adminLogout');


    Route::get('/home', 'Admin\AdminHomeController@index')->name('adminHome');

    Route::prefix('/site-customer')->group(function (){
        Route::get('/', 'Admin\AdminSiteCustomerController@index')->name('adminSiteCustomer');
        Route::post('/', 'Admin\AdminSiteCustomerController@save')->name('adminSiteCustomer');
    });

    Route::prefix('/contact')->group(function(){
        Route::get('/', 'Admin\AdminContactController@index')->name('adminContact');
        Route::post('/', 'Admin\AdminContactController@save')->name('adminContact');
        Route::get('/delete/{id}', 'Admin\AdminContactController@delete')->name('adminContactDelete');
    });

    Route::prefix('/works')->group(function (){
        Route::get('/', 'Admin\AdminWorkController@index')->name('adminWorks');

        Route::prefix('/add')->group(function (){
            Route::get('/', 'Admin\AdminWorkController@add')->name('adminWorkAdd');
            Route::post('/', 'Admin\AdminWorkController@addSave')->name('adminWorkAdd');
            Route::post('/images', 'Admin\AdminWorkController@saveImages')->name('adminWorkAddImages');
        });

        Route::prefix('/edit')->group(function (){
            Route::get('/{id}', 'Admin\AdminWorkController@edit')->name('adminWorkEdit');
            Route::post('/{id}', 'Admin\AdminWorkController@editSave')->name('adminWorkEdit');
//            Route::delete('/{id}', 'Admin\AdminWorkController@delete')->name('adminWorkEdit');

            Route::delete('/image', 'Admin\AdminWorkController@deleteImage')->name('adminWorkEditImage');
            Route::put('/image', 'Admin\AdminWorkController@attImage')->name('adminWorkEditImage');
        });
    });

});