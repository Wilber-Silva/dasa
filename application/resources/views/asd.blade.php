<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>PDGL - Transporte e Logistica</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">


    <link rel="stylesheet" href="{{asset("assets/css/iconfont.css")}}">
    <link rel="stylesheet" href="{{asset("assets/css/slick/slick.css")}}">
    <link rel="stylesheet" href="{{asset("assets/css/slick/slick-theme.css")}}">
    <link rel="stylesheet" href="{{asset("assets/css/stylesheet.css")}}">
    <link rel="stylesheet" href="{{asset("assets/css/font-awesome.min.css")}}">
    {{--<link rel="stylesheet" href="{{asset("assets/css/jquery.fancybox.css")}}">--}}
    <link rel="stylesheet" href="{{asset("assets/css/magnific-popup.css")}}">
    <link rel="stylesheet" href="{{asset("assets/css/bootstrap.min.css")}}">
    <link rel="stylesheet" href="{{asset("assets/css/plugins.css")}}" />
    <link rel="stylesheet" href="{{asset("assets/css/style.css")}}">
    <link rel="stylesheet" href="{{asset("assets/css/responsive.css")}}" />

    <style>
        .modal-dialog{
            z-index: 9999!important;
        }
        #regionTab a{
            font-size: 1.4em;
            transition: background-color .5s;
            -webkit-text-replace: background-color .5s;
        }
        #regionTab a:hover{
            background-color: #e74c3c;
            color:#fff;
        }
        #regionTab .active a{
            background-color: #e74c3c;
            color:#fff;
        }
        .tab-content{
            display:none;
            padding-top: 30px;
        }
        .tab-content .deadline{
            font-size: 1.2em;
            font-weight: bold;

        }
        .tab-content .distance{
            font-size: 1em;
        }

        .content-cities-table{
            overflow: auto;
            max-height: 250px;
        }
    </style>

</head>
<body data-spy="scroll" data-target="navbar-collapse">

<!-- Modal -->
<div id="answered-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" style="font-size: 3em;">&times;</button>
                <h4 id="answered-type" class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <div id="answered-body">
                    <ul id="regionTab" class="nav nav-tabs">

                    </ul>

                    <div id="tab-body-area"></div>
                </div>
            </div>
            {{--<div class="modal-footer">--}}
                {{--<button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>--}}
            {{--</div>--}}
        </div>

    </div>
</div>

<div class='preloader'><div class='loaded'>&nbsp;</div></div>
<div class="culmn">
    <header id="main_menu" class="header navbar-fixed-top">
        <div class="main_menu_bg">
            <div class="container">
                <div class="row">
                    <div class="nave_menu">
                        <nav class="navbar navbar-default">
                            <div class="container-fluid">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="#home">
                                        <img src="{{asset("assets/images/logoPDLG.png")}}" style="max-height: 70px!important;margin-top:-10px;"/>
                                    </a>
                                </div>

                                <!-- Collect the nav links, forms, and other content for toggling -->



                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                                    <ul class="nav navbar-nav navbar-right">
                                        <li><a href="#home">Inicio</a></li>
                                        <li><a href="#history">Sobre Nós</a></li>
                                        <li><a href="#portfolio">Galeria</a></li>
                                        <li><a href="#contact">Contato</a></li>
                                        <li>
                                            <div class="dropdown">
                                                <a class="dropdown-toggle" type="button" data-toggle="dropdown">Praças Atendidas
                                                    <span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    @foreach(squares_answered() as $index => $state)
                                                        <li><a href="javascript:void(0)" data-toggle="modal" data-target="#answered-modal" data-state="{{$index}}" data-state-name="{{$state}}">{{$state}}</a></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </li>
                                        <li><a href="{{PDLG_SYSTEM}}">Rastreamento</a></li>
                                    </ul>


                                </div>

                            </div>
                        </nav>
                    </div>
                </div>

            </div>

        </div>
    </header> <!--End of header -->




    <!--home Section -->
    <section id="home" class="home">
        <div class="overlay">
            <div class="home_skew_border">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="main_home_slider text-center">
                                <div class="single_home_slider">
                                    <div class="main_home wow fadeInUp" data-wow-duration="700ms">
                                        <h1></h1>
                                        <h3></h3>
                                        {{--<div class="separator"></div>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="scrooldown">
                    <a href="#feature"><i class="fa fa-arrow-down"></i></a>
                </div>
            </div>
        </div>
    </section><!--End of home section -->


    <!--feature section -->
    <section id="feature" class="feature sections">
        <div class="container">
            <div class="row">
                <div class="main_feature text-center">

                    <div class="col-sm-3">
                        <div class="single_feature">
                            <div class="single_feature_icon">
                                <i class="fa fa-map-marker"></i>
                            </div>

                            <h4>LOCALIZAÇÃO Estratégica</h4>
                            <div class="separator3"></div>
                            <p>Estamos localizados a apenas 9km do aeroporto</p>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="single_feature">
                            <div class="single_feature_icon">
                                <i class="fa fa-cube"></i>
                            </div>

                            <h4>Área</h4>
                            <div class="separator3"></div>
                            <p>Área total de 30.500m².</p>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="single_feature">
                            <div class="single_feature_icon">
                                <i class="fa fa-lock"></i>
                            </div>
                            <h4>Segurança</h4>
                            <div class="separator3"></div>
                            <p>Portaria Blindada, 24h com FFTV.</p>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="single_feature">
                            <div class="single_feature_icon">
                                <i class="fa fa-file-text-o"></i>
                            </div>

                            <h4>Anvisa</h4>
                            <div class="separator3"></div>
                            <p>Para armazenamento e transporte de alimentos.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div><!--End of container -->
    </section><!--End of feature Section -->
    <hr />


    <!-- History section -->
    <section id="history" class="history sections">
        <div class="container">
            <div class="row">
                <div class="main_history">
                    <div class="col-sm-6">
                        <div class="single_history_img">
                            <img src="{{asset("assets/images/about_us.jpg")}}" alt="" />
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="single_history_content">
                            <div class="head_title">
                                <h2>Sobre nós</h2>
                            </div>
                            <div id="about_us_body">
                                Para a PDLG, a excelência no transporte é muito mais do que nossa missão: é nossa forma de trabalhar todos os dias. Com seu atendimento rápido, ágil e seguro, tem sido possível trazer a seus clientes a certeza de um grande negócio. Através de sua equipe engajada pela busca da excelência em servir é que a PDLG tem conquistado o respeito e reconhecimento de seus clientes, parceiros, colaboradores e sociedade.
                            </div>

                        </div>
                    </div>
                </div>
            </div><!--End of row -->
        </div><!--End of container -->
    </section><!--End of history -->

    @php
        $defaultGalleryObj = (object) [
            "title" => "PDLG",
            "description" => "Transporte e Logistica"
        ];

        $faucetGalleryImages  = [
            (object) ["img" => "faucet/1.jpg", "type" => "faucet"],
            (object) ["img" => "faucet/2.jpg", "type" => "faucet"],
            (object) ["img" => "faucet/3.jpg", "type" => "faucet"],
            (object) ["img" => "faucet/4.jpg", "type" => "faucet"],
        ];

        $depositGalleryImages = [
            (object) ["img" => "deposit/1.jpg", "type" => "deposit"],
            (object) ["img" => "deposit/2.jpg", "type" => "deposit"],
            (object) ["img" => "deposit/3.jpg", "type" => "deposit"],
            (object) ["img" => "deposit/4.jpg", "type" => "deposit"],
            (object) ["img" => "deposit/5.jpg", "type" => "deposit"],
            (object) ["img" => "deposit/6.jpg", "type" => "deposit"],
            (object) ["img" => "deposit/7.jpg", "type" => "deposit"],
            (object) ["img" => "deposit/8.jpg", "type" => "deposit"],
            (object) ["img" => "deposit/9.jpg", "type" => "deposit"],
        ];

        shuffle($faucetGalleryImages);
        shuffle($depositGalleryImages);

        $galleryImages = [
            $faucetGalleryImages,
            $depositGalleryImages
        ];

        shuffle($galleryImages)
    @endphp
    <section id="portfolio" class="portfolio sections">
        <div class="container-fluid">
            <div class="row">
                <div class="main_portfolio">
                    <div class="col-sm-12">
                        <div class="head_title text-center">
                            <h2>Galeria</h2>
                            <div class="subtitle">
                                It has survived not only five centuries, but also the leap scrambled it to make a type.
                            </div>
                            <div class="separator"></div>
                        </div>
                    </div>
                    <div class="work_menu text-center">
                        <div id="filters" class="toolbar mb2 mt2">
                            <button class="btn-md fil-cat filter active" data-filter="all">Todas</button>/
                            <button class="btn-md fil-cat filter" data-rel="faucet" data-filter=".faucet">Fachada</button>/
                            <button class="btn-md fil-cat filter" data-rel="deposit" data-filter=".deposit">Depósito</button>
                            {{--<button class="btn-md fil-cat filter" data-rel="flyers" data-filter=".flyers">ANIMATION</button>/--}}
                            {{--<button class="btn-md fil-cat filter" data-rel="bcards" data-filter=".bcards">ART</button>/--}}
                            {{--<button class="btn-md fil-cat filter" data-rel="photo" data-filter=".photo">PHOTOGRAPHY</button>/--}}
                            {{--<button class="btn-md fil-cat filter" data-rel="video" data-filter=".video">VIDEO</button>--}}
                        </div>

                    </div>

                    <div style="clear:both;"></div>
                    <div id="portfoliowork">
                        @foreach($galleryImages as $images)
                            @foreach($images as $image)
                                <div class="single_portfolio tile scale-anm web grid-item-width2 {{$image->type}}" >
                                    <img src="{{asset("assets/images/$image->img")}}" alt="" />
                                    <a href="{{asset("assets/images/$image->img")}}" class="portfolio-img">
                                        <div class="grid_item_overlay" style="background-color: rgba(0,0,0, 0.5)">
                                            {{--<div class="separator4"></div>--}}
                                            {{--<h3>{{$image->title}}</h3>--}}
                                            {{--<p>{{$image->description}}</p>--}}
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        @endforeach


                    <div style="clear:both;"></div>
                </div>
            </div>
        </div><!-- End off container -->
    </section> <!-- End off Work Section -->

    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="contact_contant sections">
                        <div class="head_title text-center">
                            <h2>Contato</h2>
                            <div class="subtitle">
                                Entre em contato conosco, e faça um orçamento
                            </div>
                            {{--<div class="separator"></div>--}}
                        </div><!-- End off Head_title -->

                        <div class="row">
                            <div style="padding-left: 40px; padding-right: 40px; margin-bottom: 20px;" class="col-sm-12 col-md-12 col-lg-12 center-block">
                                <div class="mapouter center-block">
                                    <div class="gmap_canvas">
                                        <iframe width="1080" height="204" id="gmap_canvas" src="https://maps.google.com/maps?q=Av%Papa%Joao%Paulo%I%0004006%Cumbica%Guarulhos%SP%Brasil&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                        <a href="https://www.crocothemes.net"></a>
                                    </div>
                                    <style>
                                        .mapouter{text-align:right;height:100%;width:100%;}
                                        .gmap_canvas {overflow:auto;background:none!important;height:230px;width:100%;}
                                    </style>
                                    <small>
                                        <a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=28532&url=42158&tid1=google1" rel="nofollow"></a>
                                    </small>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="main_contact_info">
                                    <div class="row">
                                        <div class="contact_info_content padding-top-90 padding-bottom-60 p_l_r">
                                            <div class="col-sm-12">
                                                <div class="single_contact_info">
                                                    <div class="single_info_text">
                                                        <h3>Localização</h3>
                                                        <h4>Av Papa Joao Paulo I,4006 Galpao 2 Mod 2 - Bairro Cumbica, Guarulhos - SP</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="single_contact_info">
                                                    <div class="single_info_text">
                                                        <h3>Telefone de ocntato</h3>
                                                        <h4>(11) 2279-5907 / 2279-5556</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="single_contact_info">
                                                    <div class="single_info_text">
                                                        <h3>E-mail de contato</h3>
                                                        <h4>contato@pdlg.com.br</h4>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="single_contant_left padding-top-90 padding-bottom-90">
                                    <form action="#" id="formid">
                                        <div class="col-lg-8 col-md-8 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-1">

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="name" placeholder="Seu nome" required="">
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="email" class="form-control" name="email" placeholder="Email" required="">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="subject" placeholder="Assunto" required="">
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <textarea class="form-control" name="message" rows="7" placeholder="Mensagem"></textarea>
                                            </div>

                                            <div class="">
                                                <button type="submit" class="btn btn-lg"><i class="fa fa-envelope"></i> Enviar E-mail</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End of contact section -->

    <!--Footer section-->
    <section class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main_footer">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="flowus">
                                    <a href="https://www.facebook.com/pdlgtransportes/" target="_blank"><i class="fa fa-facebook"></i></a>
                                    {{--<a href=""><i class="fa fa-twitter"></i></a>--}}
                                    {{--<a href=""><i class="fa fa-google-plus"></i></a>--}}
                                    {{--<a href=""><i class="fa fa-instagram"></i></a>--}}
                                    {{--<a href=""><i class="fa fa-youtube"></i></a>--}}
                                    {{--<a href=""><i class="fa fa-dribbble"></i></a>--}}
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="copyright_text">
                                    <p class=" wow fadeInRight" data-wow-duration="1s"><a href="javascript:void(0)">PDLG - 2018</a> Todos os direitos reservados</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End off footer Section-->


</div>


<!-- START SCROLL TO TOP  -->

<div class="scrollup">
    <a href="#"><i class="fa fa-chevron-up"></i></a>
</div>
@include("anchor")
<script src="{{ asset("assets/js/vendor/jquery-1.11.2.min.js") }}"></script>
<script src="{{ asset("assets/js/vendor/bootstrap.min.js") }}"></script>
<script src="{{ asset("assets/js/jquery.magnific-popup.js") }}"></script>
<script src="{{ asset("assets/js/jquery.mixitup.min.js") }}"></script>
<script src="{{ asset("assets/js/jquery.easing.1.3.js") }}"></script>
<script src="{{ asset("assets/js/jquery.masonry.min.js") }}"></script>
<script src="{{ asset("assets/css/slick/slick.js") }}"></script>
<script src="{{ asset("assets/css/slick/slick.min.js") }}"></script>
<script src="{{ asset("assets/js/plugins.js") }}"></script>
<script src="{{ asset("assets/js/main.js") }}"></script>

<script>

    $(document).ready(function () {
        $("ul.nav.navbar-nav.navbar-right > li .dropdown>.dropdown-menu li>a").click(function () {
            $("#answered-type").text($(this).data("state-name"));

            makeRegionView($(this).data("state"));

            $("#answered-modal").modal("show");
        });

        console.log(citiesAnswered);
    });

    function makeRegionView(region) {
        var regions = getRegion(region);
        var tabControl = $("#regionTab");
        var tabControlItem = "";
        var tabArea = $("#tab-body-area");
        var tabs = "";

        tabArea.html("");
        tabControl.html("");

        if(regions)
            regions.forEach(function (item, index) {
                console.log(index, item);

                tabControlItem += "<li class='item item-"+index+"' data-target='#tab-content-"+index+"'><a href='javascript:void(0)'>"+item.holder+"</a></li>";

                tabs = "<div id='tab-content-"+index+"' class='tab-content'>" +
                            "<p style='text-align: center;'>" +
                                // "<span class='distance'>"+item.distance+"</span>" +
                                // "</br>" +
                                // "<span class='deadline'>"+(item.deadline ? "Prazo de entrega "+item.deadline+"": "")+"</span>" +
                            "</p>" +
                            "<div class='content-cities-table'>" +
                                "<table class='table table-bordered table-hover table-responsive content-cities-table-"+index+"'></table>" +
                            "</div>" +
                        "</div>";

                tabArea.append(tabs);

                var table = $(".content-cities-table-"+index);
                var tableItem = "";
                if(table) table.html("");

                if(item.cities){

                    var cities = item.cities.sort(function (a,b) {;
                        if (a < b) return -1;
                        if (a > b) return 1;
                        return 0;
                    });

                    cities.forEach(function (itemCity, indexCity) {

                        tableItem += "<tr class='content-line-region"+region+"-index"+index+"-city"+indexCity+"'>" +
                                        "<td>"+itemCity+"</td>" +
                                    "</tr>";
                    });

                    table.append(tableItem);
                }
            });

        tabControl.append(tabControlItem);


        $(".item-0").addClass("active");
        $("#tab-content-0").show(150);

        $(".nav-tabs li").click(function () {
            $(".nav-tabs li").removeClass("active");
            $(this).addClass("active");
            $(".tab-content").hide();
            $($(this).data("target")).show(150);
        });
    }

    function getRegion(state) {
        return citiesAnswered[state];
    }
</script>

</body>
</html>
