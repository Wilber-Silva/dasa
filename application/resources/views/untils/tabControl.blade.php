<script>

    $(document).ready(function () {

        @if(old('tab'))
        $('.nav-tabs li').removeClass('active');
        $('{{old('tab')}}').addClass('active');
                @endif

        var targetTab = $('.nav-tabs > .active').data('target');
        $(targetTab).show();

        $('.nav-tabs > li').click(function () {
            var targetTab = $(this).data('target');

            $('.nav-tabs li').removeClass('active');
            $('.tab').hide();

            $(this).addClass('active');

            $(targetTab).show();

            $("#tab").val("#" + $(this).attr('id'));
        });
    });
</script>