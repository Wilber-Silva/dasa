<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div style="font-size: 1.6em;color: #ffffff;padding:1em">
                <div id="modal-title" style="font-size: 1em;font-weight:bold;padding-bottom:1em"></div>
                <div id="modal-text" style="font-size: 0.8em"></div>

                <div style="text-align:center;padding-top:1em;">
                    <button type="button" id="noreload" class="btn" data-dismiss="modal" style="font-weight:bold;color:#000000;background-color: #ffffff">OK</button>
                    <button type="button" id="reload" class="btn" style="font-weight:bold;color:#000000;background-color: #ffffff" onclick="location.reload()">OK</button>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    function displayModal(title, message, type = 'success', align = 'center', reload = false){
        if (type == 'success') {
            color = '#3CB371';
        } else if (type == 'error') {
            color = '#B22222';
        }
        if (reload == true) {
            $('#reload').show();
            $('#noreload').hide();
        } else {
            $('#reload').hide();
            $('#noreload').show();
        }

        if($('#modalInfo')){
            $('#modalInfo .modal-content').css('background-color', color);
            $('#modalInfo #modal-content').css('text-align', align);
            $('#modalInfo #modal-title').text(title);
            $('#modalInfo #modal-text').html(message);
            console.log('modal');
            $('#modalInfo').modal('show');
            $('#layer').remove();
        }
    }

    @if(Session::has('response'))
        <?php $response = Session::get('response')?>
        var message = '';

        @foreach($response->messages as $message)
            message += "<li>{{$message}}</li>";
        @endforeach


        displayModal("{{$response->title}}", message, "{{$response->status}}", "{{$response->align}}");
    @endif

</script>