<ul class="sidebar-menu" data-widget="tree">
    <li class="header">Menu Principal</li>

    <li id="{{\App\Untils\Pages::CONTENT['home']['code']}}" class="menu redirect">
        <a href="{{url()->route('adminHome')}}">
            <i class="fa fa-home"></i> <span>Inicio</span>
        </a>
    </li>

    <li id="{{\App\Untils\Pages::CONTENT['siteCustomer']['code']}}" class="menu redirect">
        <a href="{{url()->route('adminSiteCustomer')}}">
            <i class="fa fa-edit"></i> <span>Site</span>
        </a>
    </li>

    <li id="{{\App\Untils\Pages::CONTENT['work']['code']}}" class="menu redirect">
        <a href="{{url()->route('adminWorks')}}">
            <i class="fa fa-building"></i> <span>Obras</span>
        </a>
    </li>

    <li id="{{\App\Untils\Pages::CONTENT['contact']['code']}}" class="menu redirect">
        <a href="{{url()->route('adminContact')}}">
            <i class="fa fa-headphones"></i> <span>Contato</span>
        </a>
    </li>
    <!--menu-open-->

    <li id="" class="menu redirect">
        <a href="#">
            <i class="fa fa-globe"></i> <span>Histórico do sistema</span>
        </a>
    </li>


    {{--<li class="header">Vencimento da licença --}}{{-- limit licence --}}{{-- </li>--}}
</ul>


{{--<li class="menu treeview">--}}
    {{--<a href="#">--}}
        {{--<i class="fa fa-bank"></i> <span>Financeiro</span>--}}
        {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
    {{--</a>--}}
    {{--<ul class="treeview-menu">--}}
        {{--<li id="search_finance_all"><a href="#"><i class="fa fa-circle-o"></i> Completo</a></li>--}}
        {{--<li data-toggle="modal" data-target="#filter-date"><a href="#"><i class="fa fa-circle-o"></i> Por data</a></li>--}}
    {{--</ul>--}}
{{--</li>--}}