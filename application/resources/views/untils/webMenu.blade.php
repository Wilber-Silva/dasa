<nav id="colorlib-main-nav" role="navigation">
    <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle active"><i></i></a>
    <div class="js-fullheight colorlib-table">
        <div class="colorlib-table-cell js-fullheight">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li id="{{\App\Library\Constants::WEB_PAGE_HOME}}"><a href="{{url()->route('index')}}">Ínicio</a></li>
                        <li id="{{\App\Library\Constants::WEB_PAGE_ABOUT}}"><a href="{{url()->route('about')}}">Sobre nós</a></li>
                        <li id="{{\App\Library\Constants::WEB_PAGE_PLANS}}"><a href="{{url()->route('plans')}}">Planos</a></li>
                        <li><a href="contact.html">Contact</a></li>

                        <li>
                            <div class="dropdown">
                                <span class="dropdown-toggle" data-toggle="dropdown">Minha Conta <i class="fa fa-angle-down"></i></span>
                                <ul class="dropdown-menu">
                                    <li><a href="{{url()->route('dentistSignIn')}}">Sou um Dentista</a></li>
                                    <li><a href="{{url()->route('dentistFunctionarySignIn')}}">Sou um Atendente</a></li>
                                    <li><a href="{{url()->route('clinicSignIn')}}">Clinica</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>


<script>
    $(document).ready(function () {
            $("#{{$currentPage}}").addClass("active");
    })
</script>