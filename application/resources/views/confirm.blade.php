<html>

<head><meta charset="utf-8">

    
    <link type="text/css" rel="stylesheet" href="/drawing/resources/css/bcPaint.css"/>
    <link type="text/css" rel="stylesheet" href="/drawing/resources/css/bcPaint.mobile.css"/>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
</head>

<body style="background-color: #E6E8FA;">

<h1 style="text-align: center;">Olá Mona Shinba</h1>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Exame:</b> Hermograma completo
                </div>
                <div class="panel-body">
                
                    <hr>
                    <p style="text-align: center">16/12/2018</p>
    
                    <hr>
                    
                    <p style="text-align: left">Plano de saúde : Unimed</p>
                    
                    <br/>
    
    
                    <div class="pull-right">
                        
                        <a href="{{URL::Route('ops')}}">
                            <button class="btn btn-danger">Não é meu exame</button>
                        </a>
                        
                        <a href="{{URL::Route('drawing')}}">
                            <button class="btn btn-success">Confirmar</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</body>

<script src="jquery.js"></script>

<script>
</script>
</html>